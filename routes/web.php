<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/farms', 'FarmController@index')->name('farms.index');
Route::get('/farms/create', 'FarmController@create')->name('farms.create');
Route::post('/farms', 'FarmController@store')->name('farms.store');
Route::get('/farms/{id}/show', 'FarmController@show')->name('farms.show');
Route::get('/farms/{id}/edit', 'FarmController@edit')->name('farms.edit');
Route::put('/farms/{id}', 'FarmController@update')->name('farms.update');
Route::delete('farms', 'FarmController@destroy')->name('farms.destroy');

Route::get('/unicorns', 'FarmController@index')->name('unicorns.index');
Route::get('/unicorns/{id?}/create', 'FarmController@create')->name('unicorns.create');
Route::post('/unicorns', 'FarmController@store')->name('unicorns.store');
Route::get('/unicorns/{id}/show', 'FarmController@show')->name('unicorns.show');
Route::get('/unicorns/{id}/edit', 'FarmController@edit')->name('unicorns.edit');
Route::put('/unicorns/{id}', 'FarmController@update')->name('unicorns.update');
Route::delete('unicorns', 'FarmController@destroy')->name('unicorns.destroy');