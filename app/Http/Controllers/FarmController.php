<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\Farm;
use App\Models\Unicorn;

class FarmController extends Controller
{
    public function index()
    {
        $auth = Auth::user();
        //demande d'identification si l'utilisateur n'est pas connecté en cas de refresh avec perte de session
        if(is_null($auth))
        {
            return view('farms.login');
        }

        $farms = Farm::where('user_id', Auth::id())->get();

        return view('farms.index', compact('farms', 'auth'));
    }

    public function create()
    {
        return view('farms.create');
    }

    public function store(Request $request)
    {
        $farm = new Farm();
        $farm->name = $request->get('name');
        $farm->description = $request->get('description');
        $farm->user_id = Auth::id();
        $farm->save();

        return redirect()->route('farms.index');
    }

    public function show($id)
    {
        $farm = Farm::with('unicorns') ->find($id);

        return view('farms.show', compact('farm'));
    }

    public function edit($id)
    {
        $farm = Farm::find($id);

        return view('farms.edit', compact('farm'));
    }

    public function update(Request $request, $id)
    {
        $farm = Farm::find($id);
        $farm->name = $request->get('name');
        $farm->description = $request->get('description');
        $farm->save();

        return redirect()->route('farms.show', $id);
    }

    public function destroy(Request $request)
    {
        Farm::destroy($request->get('id'));

        return redirect()->route('farms.index');
    }
}
