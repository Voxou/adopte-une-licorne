<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\Unicorn;
use App\Models\Farm;

class UnicornController extends Controller
{
    public function index()
    {
        $unicorns = Unicorn::all();

        return view('unicorns.index', compact('$unicorns'));
    }

    public function create($id = null)
    {
        $auth = Auth::user();

        return view('unicorns.create', compact('auth', 'id'));
    }

    public function store(Request $request)
    {
        $unicorn = new Unicorn();
        $unicorn->name = $request->get('name');
        $unicorn->gender = $request->get('gender');
        $unicorn->age = $request->get('age');
        $unicorn->breedingStart = $request->get('breedingStart');
        $unicorn->breedingEnd = $request->get('breedingEnd');
        $unicorn->picUrl = $request->get('picUrl');
        $unicorn->description = $request->get('description');
        $unicorn->price = $request->get('price');
        $unicorn->user_id = Auth::id();
        $unicorn->farm_id = $request->get('farm_id');
        $unicorn->save();

        return redirect()->route('unicorns.index');
    }

    public function show($id)
    {
        $auth = Auth::user();
        $unicorn = Unicorn::find($id);

        return view('unicorns.show', compact('auth','unicorn'));
    }

    public function edit($id)
    {
        $unicorn = Unicorn::find($id);

        return view('unicorns.edit', compact('unicorn'));
    }

    public function update(Request $request, $id)
    {
        $unicorn = Farm::find($id);
        $unicorn->name = $request->get('name');
        $unicorn->gender = $request->get('gender');
        $unicorn->age = $request->get('age');
        $unicorn->breedingStart = $request->get('breedingStart');
        $unicorn->breedingEnd = $request->get('breedingEnd');
        $unicorn->picUrl = $request->get('picUrl');
        $unicorn->description = $request->get('description');
        $unicorn->price = $request->get('price');
        $unicorn->farm_id = $request->get('farm_id');
        $unicorn->save();

        return redirect()->route('unicorns.show', $id);
    }

    public function destroy(Request $request)
    {
        Unicorn::destroy($request->get('id'));

        //TO DO
        return redirect()->route('unicorns.index');
    }
}
