<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Farm extends Model
{
    protected $table = 'farms';

    protected $fillable = [
        'id', 'name', 'description', 'user_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function unicorns()
    {
        return $this->hasMany(Unicorn::class);
    }
}
