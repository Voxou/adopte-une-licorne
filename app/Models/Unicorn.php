<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Unicorn extends Model
{
    protected $table = 'unicorns';

    protected $fillable = [
        'id', 'name', 'gender', 'age', 'breedingStart', 'breedingEnd', 'picUrl', 'description', 'price', 'user_id','farm_id',
    ];

    public  function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function farm()
    {
        return $this->belongsTo(Farm::class, 'farm_id');
    }
}
