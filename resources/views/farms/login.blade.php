@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Vous n'êtes pas/plus identifié</div>

                    <div class="card-body">
                        <p>Veuillez créer un compte ou vous connectez si vous en possedez déjà un.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection