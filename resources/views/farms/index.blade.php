@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Bonjour {{ $auth->name }}</div>

                    <div class="card-body">
                        <p>
                            Votre type de compte :
                            @switch($auth->type)
                                @case('farmer')
                                <span>éleveur</span>
                                @break
                                @case('breeder')
                                <span>reproducteur</span>
                                @break
                                @case('buyer')
                                <span>acheteur</span>
                                @break
                                @default
                                <span>type inconnu</span>
                            @endswitch
                        </p>

                        @if($auth->type == 'farmer')
                            <a href="{{ route('farms.create') }}" class="btn btn-primary">Créer un élevage</a>
                            @if($farms->isEmpty())
                                <p>Vous n'avez pas encore d'élevage enregistré</p>
                            @else
                                <p>Votre/vos élevage(s)</p>
                                <ul>
                                    @foreach($farms as $farm)
                                        <li>
                                            <a href="{{ route('farms.show', $farm->id) }}">{{ $farm->name }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                        @endif

                        <a href="{{ route('home') }}" class="btn btn-secondary">Retour</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
