@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ $farm->name }}</div>

                    <div class="card-body">
                        <p>{{ $farm->description }}</p>

                        <ul>
                            @foreach($farm->unicorns as $unicorn)
                                <li><a href="{{ route('$unicorns.show', $unicorn->id) }}">{{ $unicorn->name }}</a></li>
                        </ul>

                        <a href="{{ route('farms.edit', $farm->id) }}" class="btn btn-warning">Modifier l'élevage</a>

                        <a href="{{ route('unicorn.create', $farm->id) }}" class="btn btn-warning">Ajouter une licorne à cet élevage</a>

                        <form action="{{ route('farms.destroy') }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="hidden" name="id" value="{{ $farm->id }}">
                            <button class="btn btn-danger" type="submit">Supprimer</button>
                        </form>

                        <a href="{{ route('farms.index') }}" class="btn btn-secondary">Retour</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection