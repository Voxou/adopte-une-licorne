@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Création d'une licorne</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('unicorns.store') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">Nom</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="gender" class="col-md-4 col-form-label text-md-right">Genre</label>

                                <div class="col-md-6">
                                    <select id="gender" class="form-control" name="gender" required>
                                        <option value="">Choisissez un genre</option>
                                        <option value="male">Male</option>
                                        <option value="female">Femelle</option>
                                        <option value="other">Autre</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="age" class="col-md-4 col-form-label text-md-right">Âge (en années)</label>

                                <div class="col-md-6">
                                    <input id="age" type="text" class="form-control" name="age" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="breedingStart" class="col-md-4 col-form-label text-md-right">Début de la période de reproduction</label>

                                <div class="col-md-6">
                                    <input id="breedingStart" type="date" class="form-control" name="breedingStart" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="breedingEnd" class="col-md-4 col-form-label text-md-right">Fin de la période de reproduction</label>

                                <div class="col-md-6">
                                    <input id="breedingEnd" type="date" class="form-control" name="breedingEnd" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="picUrl" class="col-md-4 col-form-label text-md-right">Url de l'image (optionnel)</label>

                                <div class="col-md-6">
                                    <input id="picUrl" type="text" class="form-control" name="picUrl">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="description" class="col-md-4 col-form-label text-md-right">Description</label>

                                <div class="col-md-6">
                                    <textarea id="description" class="form-control" name="description" required></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="price" class="col-md-4 col-form-label text-md-right">Prix (en €)</label>

                                <div class="col-md-6">
                                    <input id="price" type="number" class="form-control" name="price" required>
                                </div>
                            </div>

                            <input type="hidden" name="farm_id" value="{{ $id }}">

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                                </div>
                            </div>
                        </form>

                        @if(!is_null($id))
                            <a href="{{ route('farms.show', $id) }}" class="btn btn-secondary">Retour</a>
                        @elseif
                            <a href="{{ route('unicorns.index') }}" class="btn btn-secondary">Retour</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
