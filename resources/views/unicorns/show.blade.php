@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ $unicorn->name }}</div>

                    <div class="card-body">
                        <p>Nom : {{ $unicorn->name }}</p>
                        <p>Genre : {{ $unicorn->gender }}</p>
                        <p>Âge : {{ $unicorn->age }}</p>
                        <p>Début de la période de reproduction : {{ $unicorn->breedingStart }}</p>
                        <p>Fin de la période de reproduction : {{ $unicorn->breedingEnd }}</p>
                        @if(!is_null($unicorn->picUrl))
                            <img src="{{ $unicorn->picUrl }}">
                        @endif
                        <p>{{ $unicorn->description }}</p>
                        @if($unicorn->user()->type == 'farmer')
                            <p>Pour vente</p>
                        @elseif
                            <p>Pour reproduction</p>
                        @endif
                        <p>Prix : {{ $unicorn->price }} €</p>

                        @if($auth->type != 'buyer')
                            <a href="{{ route('farms.edit', $farm->id) }}" class="btn btn-warning">Modifier</a>

                            <form action="{{ route('farms.destroy') }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="hidden" name="id" value="{{ $farm->id }}">
                                <button class="btn btn-danger" type="submit">Supprimer</button>
                            </form>
                        @endif

                        @if(!is_null($unicorn->farm_id))
                            <a href="{{ route('farms.show', $unicorn->farm_id) }}" class="btn btn-secondary">Retour</a>
                        @elseif
                            <a href="{{ route('unicorns.index') }}" class="btn btn-secondary">Retour</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection