@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Liste des licornes</div>

                    <div class="card-body">
                        <ul>
                            @foreach($unicorns as $unicorn)
                                <li>
                                    <a href="{{ route('unicorns.show', $unicorn->id) }}">{{ $unicorn->name }}</a>
                                </li>
                            @endforeach
                        </ul>

                        <a href="{{ route('home') }}" class="btn btn-secondary">Retour</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
